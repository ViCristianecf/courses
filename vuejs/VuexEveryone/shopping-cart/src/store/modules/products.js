import shop from "@/api/shop";

export default {

  namespaced: true,

  state: {
    items: []
  },

  getters: {
    // retorna todos os produtos que há no estoque
    avaliableProducts (state) {
      return state.items.filter(product => product.inventory > 0)
    },

    productIsInStock () {
      return (product) => {
        return product.inventory > 0
      }
    }
  },

  actions: {
    fetchProducts ({commit}) {
      return new Promise((resolve) => {
        // make the call
        // run setProducts mutation
        shop.getProducts(products => {
          commit('setProducts', products)
          resolve()
        })
      })
    }
  },

  mutations: {
    setProducts (state, products) {
      // update products
      state.items = products
    },

    decrementItemQuantity (state, product) {
      product.inventory--
    }
  }
}
